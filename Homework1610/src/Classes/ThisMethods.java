package Classes;

public class ThisMethods {
    void methodVoid() {
        System.out.println("Method void class1.");
        this.methodInt();
        this.methodString();
    }

    int methodInt() {
        System.out.println("Method int class1.");
        return 6;
    }

    String methodString() {
        System.out.println("Method string class1.");
        return "String";
    }

    public static void main(String [] args) {
        ThisMethods thisMethods = new ThisMethods();

        thisMethods.methodVoid();
    }
}


