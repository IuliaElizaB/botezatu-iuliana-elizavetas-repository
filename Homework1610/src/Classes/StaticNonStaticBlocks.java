package Classes;

public class StaticNonStaticBlocks {
    static int num;
        static {
            num = 7;
        }

        {
            num = 8;
        }

    public static void main(String [] args) {
        System.out.println("Num = " + num);
    }
}
