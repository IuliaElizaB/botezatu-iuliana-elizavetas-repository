package Classes;

public class ThisParameter {
    public ThisParameter() {
        printParameter(this);
    }

    private void printParameter(ThisParameter thisParameter) {
        System.out.println(thisParameter);
    }
    
    public static void main(String [] args) {
        ThisParameter thisParameter = new ThisParameter();
        thisParameter.printParameter();
    }

    private void printParameter() {
    }
}
