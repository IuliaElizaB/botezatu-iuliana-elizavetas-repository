package Classes;

public class OverloadedThisConstructor {
    private String name;
    private String surname;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public OverloadedThisConstructor() {
        this.name = "Iulia";
    }

    public OverloadedThisConstructor(String surname2) {
        this();
        this.surname = surname2;
    }

    public OverloadedThisConstructor(String surname3, int age3) {
        this();
        this.surname = surname3;
        this.age = age3;
    }

    public static void main(String [] args) {
        OverloadedThisConstructor overloadedThisConstructor = new OverloadedThisConstructor();
        System.out.println("Constructor 1");
        System.out.println("   Name: " + overloadedThisConstructor.getName());
        System.out.println("   Surname: " + overloadedThisConstructor.getSurname());
        System.out.println("   Age: " + overloadedThisConstructor.getAge());
        System.out.println();

        OverloadedThisConstructor overloadedThisConstructor1 = new OverloadedThisConstructor("Botezatu");
        System.out.println("Constructor 2");
        System.out.println("   Name2: " + overloadedThisConstructor1.getName());
        System.out.println("   Surname2: " + overloadedThisConstructor1.getSurname());
        System.out.println("   Age2: " + overloadedThisConstructor1.getAge());
        System.out.println();

        OverloadedThisConstructor overloadedThisConstructor2 = new OverloadedThisConstructor("Popa",45);
        System.out.println("Constructor 3");
        System.out.println("   Name3: " + overloadedThisConstructor2.getName());
        System.out.println("   Surname3: " + overloadedThisConstructor2.getSurname());
        System.out.println("   Age3: " + overloadedThisConstructor2.getAge());
    }
}
