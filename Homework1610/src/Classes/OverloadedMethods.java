package Classes;

public class OverloadedMethods {
    void sum(int var1, int var2) {
        int sum = var1 + var2;
        System.out.println(sum);
    }

    void sum(int var1, int var2, int var3) {
        int sum = var1 + var2 + var3;
        System.out.println(sum);
    }

    void sum(int var1, int var2, int var3, int var4) {
        int sum = var1 + var2 + var3 + var4;
        System.out.println(sum);
    }

    public static void main(String [] args) {
        OverloadedMethods overloadedMethods = new OverloadedMethods();
        overloadedMethods.sum(1,2);
        overloadedMethods.sum(1,2,3);
        overloadedMethods.sum(1,2,3,4);
    }
}
