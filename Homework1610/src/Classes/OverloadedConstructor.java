package Classes;

public class OverloadedConstructor {
    private String name;
    private String surname;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public OverloadedConstructor() {
        this.name = "Iulia";
        this.surname = "Botezatu";
        this.age = 19;
    }

    public OverloadedConstructor(String name2, String surname2) {
        this.name = name2;
        this.surname = surname2;
    }

    public OverloadedConstructor(String name3, String surname3, int age3) {
        this.name = name3;
        this.surname = surname3;
        this.age = age3;
    }

    public static void main(String [] args) {
        OverloadedConstructor overloadedConstructor = new OverloadedConstructor();
        System.out.println("Constructor 1");
        System.out.println("   Name: " + overloadedConstructor.getName());
        System.out.println("   Surname: " + overloadedConstructor.getSurname());
        System.out.println("   Age: " + overloadedConstructor.getAge());
        System.out.println();

        OverloadedConstructor overloadedConstructor1 = new OverloadedConstructor("Eliza", "Botezatu");
        System.out.println("Constructor 2");
        System.out.println("   Name2: " + overloadedConstructor1.getName());
        System.out.println("   Surname2: " + overloadedConstructor1.getSurname());
        System.out.println("   Age2: " + overloadedConstructor1.getAge());
        System.out.println();

        OverloadedConstructor overloadedConstructor2 = new OverloadedConstructor("Oleg", "Botezatu", 45);
        System.out.println("Constructor 3");
        System.out.println("   Name3: " + overloadedConstructor2.getName());
        System.out.println("   Surname3: " + overloadedConstructor2.getSurname());
        System.out.println("   Age3: " + overloadedConstructor2.getAge());
    }
}
