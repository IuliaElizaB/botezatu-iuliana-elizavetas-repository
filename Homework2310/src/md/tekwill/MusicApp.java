package md.tekwill;

public interface MusicApp extends Song, Sound{
    public abstract void openMusicApp();
}
