package md.tekwill;

public abstract class Phone {
    public abstract void homeButton();
    public abstract void backButton();
    public abstract void menuButton();
    public abstract void openGalleryApp();
    public abstract void openMusicApp();
    public abstract void onMusic();
    public abstract void offMusic();
    public abstract void onSound();
    public abstract void offSound();
}
