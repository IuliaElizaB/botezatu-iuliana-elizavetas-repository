package md.tekwill;

public interface Sound {
    public void onSound();
    public void offSound();
}
