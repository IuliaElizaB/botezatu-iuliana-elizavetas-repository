package md.tekwill;

public class Display extends Phone {
    @Override
    public void homeButton() {
        System.out.println("Going home...");
    }

    @Override
    public void backButton() {
        System.out.println("Going back...");
    }

    @Override
    public void menuButton() {
        System.out.println("Opening menu...");
    }

    @Override
    public void openGalleryApp() {
        System.out.println("Opening gallery app...");
    }

    @Override
    public void openMusicApp() {
        System.out.println("Opening music app...");
    }

    @Override
    public void onMusic(){
        System.out.println("Music on.");
    }

    @Override
    public void offMusic(){
        System.out.println("Music off.");
    }

    @Override
    public void onSound(){
        System.out.println("Sound on.");
    }

    @Override
    public void offSound(){
        System.out.println("Sound off.");
    }

}
