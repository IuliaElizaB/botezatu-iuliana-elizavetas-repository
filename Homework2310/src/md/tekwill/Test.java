package md.tekwill;

public class Test {
    public static void main(String[] args) {
        Phone phone = new Display();

        phone.homeButton();
        phone.menuButton();
        phone.backButton();

        System.out.println();
        System.out.println();

        phone.openGalleryApp();

        System.out.println();
        System.out.println();

        phone.openMusicApp();

        System.out.println();

        phone.onMusic();
        phone.offMusic();

        System.out.println();

        phone.onSound();
        phone.offSound();
    }
}
