package md.tekwill;

public interface Song {
    public void onMusic();
    public void offMusic();
}
