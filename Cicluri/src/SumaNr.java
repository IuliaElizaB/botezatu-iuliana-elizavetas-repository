public class SumaNr {
    public static void main(String [] args){
        int suma = 0;
        int n = 5;

        for (int i = 0; i < n; i++) {
            suma += i;
        }

        System.out.println("Suma numerelor pana la " + n  + " este " + suma);
    }
}
