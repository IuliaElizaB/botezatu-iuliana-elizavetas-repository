public class NrImpare {
    public static void main(String[] args) {
        int n = 5;

        System.out.println("Nr impare pana la " + n + " sunt: ");

        for (int i = 0; i < n; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
    }
}
