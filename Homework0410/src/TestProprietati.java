public class TestProprietati {
    public static void main(String [] args) {
        MetodeProprietati proprietati = new MetodeProprietati();

        proprietati.name = "Iulia";
        proprietati.age = 19;
        proprietati.gender = 'f';
        proprietati.amount = 2000.80f;

        String result1 = proprietati.toString();
        System.out.println(result1);

        System.out.println();

        int result2 = proprietati.IntMethod(6);
        System.out.println(result2);

        System.out.println();

        proprietati.VoidStringMethod("paramString1", "paramString2");

        System.out.println();

        String result3 = proprietati.StringMethod();
        System.out.println(result3);

        System.out.println();

        int result4 = proprietati.CharIntMethod('p');
        System.out.println(result4);
    }
}
