public class MetodeProprietati {
    String name;
    int age;
    char gender;
    float amount;

    public String toString() {
        System.out.println("Obiectele clasei.");

        return name + ", " + age + ", " + gender + ", " +  amount;
    }

    int IntMethod(int paramInt) {
        System.out.println("Metoda ce returneaza int cu 1 parametri tip int.");

        return paramInt;
    }

    void VoidStringMethod(String paramString1, String paramString2) {
        System.out.println("Metoda ce nu returneaza nimic cu 2 parametri tip string.");
    }

    String StringMethod() {
        System.out.println("Metoda ce returneaza string cu 0 parametri.");

        return "This is string.";
    }

    int CharIntMethod(char paramChar) {
        System.out.println("Metoda ce returneaza int cu 1 parametri tip char.");

        return paramChar;
    }
}
