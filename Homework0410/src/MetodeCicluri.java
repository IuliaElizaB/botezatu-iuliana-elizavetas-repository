public class MetodeCicluri {
    int n;

    public void SumaNr(int suma) {
            suma = 0;

            for (int i = 0; i < n; i++) {
                suma += i;
            }

            System.out.println("Suma numerelor pana la " + n  + " este " + suma);
        }

    public void NrPare() {
            System.out.println("Nr pare pana la " + n + " sunt: ");

            for (int i = 0; i <= n; i++) {
                if (i % 2 == 0) {
                    System.out.println(i);
                }
            }
    }

    public void TenHello() {
            for (int i = 1; i <= 10; i++) {
                System.out.println(i + ") Hello");
            }
    }

    public void NrImpare() {
            System.out.println("Nr impare pana la " + n + " sunt: ");

            for (int i = 0; i < n; i++) {
                if (i % 2 != 0) {
                    System.out.println(i);
                }
            }
    }

}

