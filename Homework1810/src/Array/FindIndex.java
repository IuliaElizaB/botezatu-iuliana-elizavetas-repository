package Array;

public class FindIndex {

        public static int findIndex(int arr[], int j)
        {
            if (arr == null) {
                return -1;
            }

            int i = 0;

            while (i < arr.length) {
                if (arr[i] == j) {
                    return i;
                }
                else {
                    i++;
                }
            }
            return -1;
        }

        public static void main(String[] args)
        {
            int[] myArray = {8, 7, 1, 6, 5, 3, 4, 2};

            System.out.println("Index position of 5 is: "
                    + findIndex(myArray, 5));

            System.out.println("Index position of 7 is: "
                    + findIndex(myArray, 7));
        }

}
