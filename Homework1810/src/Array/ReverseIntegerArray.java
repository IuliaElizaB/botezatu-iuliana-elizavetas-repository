package Array;

public class ReverseIntegerArray {

    static void reverse(int arr[], int n)
    {
        int[] arr1 = new int[n];
        int j = n;
        for (int i = 0; i < n; i++) {
            arr1[j - 1] = arr[i];
            j = j - 1;
        }

        System.out.println("Reversed array is: ");
        for (int k = 0; k < n; k++) {
            System.out.print(arr1[k]+ " ");
        }
    }

    public static void main(String[] args)
    {
        int [] arr = {0, 1, 2, 3, 4, 5};
        reverse(arr, arr.length);
    }

}
