package Inheritance;

public class Parent {
    protected String name;
    protected String surname;
    protected int age;

    String informationMethod() {
        String information = name + " " + surname + " " + age;
        return information;
    }
}
