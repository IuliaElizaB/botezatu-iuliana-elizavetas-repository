package Inheritance;

public class Child extends Parent {
    @Override
    String informationMethod() {
        String information = name + " " + surname + " " + age;
        return information;
    }
}
