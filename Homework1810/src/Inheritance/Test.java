package Inheritance;

public class Test {
    public static void main(String[] args) {
        Parent inheritance = new Child();
        inheritance.name = "Oleg";
        inheritance.surname = "Botezatu";
        inheritance.age = 45;

        System.out.println(inheritance.informationMethod());
    }
}
